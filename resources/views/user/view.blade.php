@extends('layouts.app')

@section('title', $title)

@section('content')
    <section id="main">
        <header>
            <span class="avatar"><img src="{{asset('assets/images/users/'. $user->id .'.jpg')}}" alt="" /></span>
            <h1 style="font-size:2em !important;">{{$user->name}}</h1>
            <p style="text-transform:none !important">
                {!!nl2br($user->comment)!!}
            </p>
            <a href="{{route('user.comment', $user->id)}}" class="btn btn-default">Add Comment</a>
        </header>
    </section>
@endsection
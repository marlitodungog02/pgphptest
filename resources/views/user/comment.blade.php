@extends('layouts.app')

@section('title', $title)

@section('content')
    <section id="main">
        <header class="text-left">
            @if ($errors->any())
                <div class="alert alert-danger" style="text-transform:none !important">
                    @foreach ($errors->all() as $error)
                        {{ $error }}
                    @endforeach
                </div>
            @endif

            @if(session()->has('message'))
                <div class="alert alert-success" style="text-transform:none !important">
                    {{ session()->get('message') }}
                </div>
            @endif

            <form action="{{route('user.store-comment', $user->id)}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" id="password" name="password" style="text-transform:none !important" required>
                </div>
                <div class="form-group">
                    <label for="comment">Comment:</label>
                    <textarea class="form-control" rows="5" id="comment" name="comment" style="text-transform:none !important" required></textarea>
                </div>
                @if($user->comment != '')
                    <div class="form-group" style="text-transform:none !important">
                        <label>Existing Comment:</label>
                        <p>
                            {!!nl2br($user->comment)!!}
                        </p>
                    </div>
                @endif
                <div class="form-group text-center">
                    <a href="{{route('user.show', $user->id)}}" class="btn btn-default">Back</a>
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </form>
        </header>
    </section>
@endsection
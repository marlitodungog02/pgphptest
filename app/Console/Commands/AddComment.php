<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Illuminate\Support\Facades\Crypt;

class AddComment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add-comment {id} {comment}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add Comment to user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::find($this->argument('id'));
        
        if ($user) {
            $user->comment = ($user->comment != '' ? $user->comment . '<br>' : '') . $this->argument('comment');
            $user->save();
    
            $this->info('User Comment Successfully Added!');
        }
        else {
            $this->info("User doesn't exists!");
        }
    }
}

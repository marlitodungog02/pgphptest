<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $title = 'User Card - ' . $user->name;

        return view('user.view', compact('title', 'user'));
    }

    public function comment($id)
    {
        $user = User::findOrFail($id);
        $title = 'User Card - ' . $user->name . ' - Comment';

        return view('user.comment', compact('title', 'user'));
    }

    public function commentStore($id, Request $request)
    {
        $user = User::findOrFail($id);

        $rules = [
            'comment' => 'required',
            'password' => ['required', function ($attribute, $value, $fail) use ($user) {
                if (!\Hash::check($value, $user->password)) {
                    return $fail(__('The password is incorrect.'));
                }
            }]
        ];
        $messages = [
            'unique' => 'Username already exists. Please choose a different username.',
        ];

        $this->validate($request, $rules, $messages);

        $user->comment = ($user->comment != '' ? $user->comment . '<br>' : '') . $request->comment;
        $user->save();

        return Redirect::route('user.comment',)->with('message', 'User Comment Successfully Added!');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
    Route::get('/{id}', 'UserController@show')->name('show');
    Route::get('/{id}/comment', 'UserController@comment')->name('comment');
    Route::post('/{id}/store-comment', 'UserController@commentStore')->name('store-comment');
});